<%--
  Created by IntelliJ IDEA.
  User: coliza
  Date: 2014/5/2
  Time: 11:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<body>
<div class="content">
    <tiles:insertAttribute name="login-content"></tiles:insertAttribute>
</div>
<div class="footer">
    <tiles:insertAttribute name="rights-footer"></tiles:insertAttribute>
</div>
</body>
</html>
