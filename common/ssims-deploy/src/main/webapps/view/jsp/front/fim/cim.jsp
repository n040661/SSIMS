<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: coliza
  Date: 2014/5/5
  Time: 23:13
--%>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<div class="nav-bar">
    <ol class="breadcrumb">
        <li class="breadcrumb-head"><a href="#">首页</a></li>
        <li class="breadcrumb-item"><a href="#">基础信息管理</a></li>
        <li class="breadcrumb-item active"><a href="#">客户信息管理</a></li>
    </ol>

</div>
<div class="query-pane"></div>
<div class="records table-responsive">
    <div class="toolbar" align="right">
        <div class="insert-btn btn btn-primary" id="insert-btn">添加信息</div>
    </div>

    <table class="table table-striped table-bordered table-hover table-condensed" width="800">
        <thead>
        <tr>
            <th width="12%" align="center">客户全称</th>
            <th width="12%" align="center">地址</th>
            <th width="8%" align="center">联系方式</th>
            <th width="6%" align="center">联系人</th>
            <th width="6%" align="center">银行账户</th>
            <th width="10%" align="center">操作</th>
        </tr>
        </thead>
        <tbody class="table-hover">
        <c:if test="${!empty records}">
            <c:forEach items="${records}" var="item" varStatus="index">
                <c:if test="${index.index%2 eq 0}">
                    <tr class="odd">
                        <td title="${item.name}">${item.name}</td>
                        <td title="${item.address}">${item.address}</td>
                        <td title="邮政编码:${item.post}&#13;电话号码:${item.phone}&#13;传真:${item.fex}&#13;电子邮件:${item.email}">
                            邮政编码:${item.post}<br>电话号码:${item.phone}<br>传真:${item.fex}<br>电子邮件:${item.email}</td>
                        <td title="联系人:${item.clientName}&#13;电话:${item.clientPhone}">
                            联系人:${item.clientName}<br>电话:${item.clientPhone}</td>
                        <td title="银行:${item.bank}&#13;账号:${item.bankAccount}">银行:${item.bank}<br>账号:${item.bankAccount}
                        </td>
                        <td align="center" valign="center">
                            <a href="javascript:void(0)"
                               onclick="showUpdateDialog('${item.id}','${item.name}','${item.shortName}','${item.address}','${item.post}','${item.phone}','${item.email}','${item.fex}','${item.clientPhone}','${item.clientName}','${item.bank}','${item.bankAccount}')">修改</a>&nbsp;
                            <a href="javascript:void(0)" onclick="deleteItem('${item.id}')">删除</a>
                        </td>
                    </tr>
                </c:if>
                <c:if test="${index.index%2 != 0}">
                    <tr class="even">
                        <td title="${item.name}">${item.name}</td>
                        <td title="${item.address}">${item.address}</td>
                        <td title="邮政编码:${item.post}&#13;电话号码:${item.phone}&#13;传真:${item.fex}&#13;电子邮件:${item.email}">
                            邮政编码:${item.post}<br>电话号码:${item.phone}<br>传真:${item.fex}<br>电子邮件:${item.email}</td>
                        <td title="联系人:${item.clientName}&#13;电话:${item.clientPhone}">
                            联系人:${item.clientName}<br>电话:${item.clientPhone}</td>
                        <td title="银行:${item.bank}&#13;账号:${item.bankAccount}">银行:${item.bank}<br>账号:${item.bankAccount}
                        </td>
                        <td align="center" valign="center">
                            <a href="javascript:void(0)"
                               onclick="showUpdateDialog('${item.id}','${item.name}','${item.shortName}','${item.address}','${item.post}','${item.phone}','${item.email}','${item.fex}','${item.clientPhone}','${item.clientName}','${item.bank}','${item.bankAccount}')">修改</a>&nbsp;
                            <a href="javascript:void(0)" onclick="deleteItem('${item.id}')">删除</a>
                        </td>
                    </tr>
                </c:if>
            </c:forEach>
        </c:if>
        </tbody>
    </table>
</div>
<!-- Start 发布对话框 -->
<div id="insertDialog">
    <table id="insert-table" cellpadding="0" cellspacing="0">
        <tbody>
        <tr>
            <td align="right" width="20%"><label>全称:</label></td>
            <td width="30%"><input type="input" class="dialog-input-name form-control" name="name"/></td>
            <td align="right" width="20%"><label>简称:</label></td>
            <td width="30%"><input type="input" class="dialog-input-shortname form-control" name="shortName"/></td>
        </tr>
        <tr>
            <td align="right"><label>地址:</label></td>
            <td colspan="3"><input type="input" class="dialog-input-address form-control" name="address"/></td>
        </tr>
        <tr>
            <td align="right"><label>邮政编码:</label></td>
            <td><input type="input" class="dialog-input-post form-control" name="post"/></td>
            <td align="right"><label>电话号码:</label></td>
            <td><input type="input" class="dialog-input-phone form-control" name="phone"/></td>
        </tr>
        <tr>
            <td align="right"><label>传真:</label></td>
            <td><input type="input" class="dialog-input-fex form-control" name="fex"/></td>
            <td align="right"><label>电子邮件:</label></td>
            <td><input type="input" class="dialog-input-email form-control" name="email"/></td>
        </tr>
        <tr>
            <td align="right"><label>联系人姓名:</label></td>
            <td><input type="input" class="dialog-input-client-n form-control" name="clientName"/></td>
            <td align="right"><label>联系人电话:</label></td>
            <td><input type="input" class="dialog-input-client-p form-control" name="clientPhone"/></td>
        </tr>
        <tr>
            <td align="right"><label>银行:</label></td>
            <td><input type="input" class="dialog-input-client-n form-control" name="bank"/></td>
            <td align="right"><label>银行账号:</label></td>
            <td><input type="input" class="dialog-input-client-p form-control" name="bankAccount"/></td>
        </tr>
        </tbody>
    </table>
</div>
<!-- 脚本 -->
<script type="text/javascript">
    $(function () {
        $("#insertDialog").dialog({
            autoOpen: false,
            title: "添加客户信息",
            modal: true,
            width: 600,
            height: 350,
            resiable: false,
            dragable: false
        });
        $("#insert-btn").click(showInsertDialog);
    });
    function showInsertDialog() {
        $("#insertDialog").dialog("option", "buttons", [
            {
                text: "添加",
                click: function () {
                    $.ajax({
                        url: "${contextPath}/fim/cim?method=insert",
                        type: "post",
                        dataType: "json",
                        data: {
                            name: $("#insert-table input[name='name']").val(),
                            shortName: $("#insert-table input[name='shortName']").val(),
                            address: $("#insert-table input[name='address']").val(),
                            post: $("#insert-table input[name='post']").val(),
                            phone: $("#insert-table input[name='phone']").val(),
                            fex: $("#insert-table input[name='fex']").val(),
                            email: $("#insert-table input[name='email']").val(),
                            clientName: $("#insert-table input[name='clientName']").val(),
                            clientPhone: $("#insert-table input[name='clientPhone']").val(),
                            bank: $("#insert-table input[name='bank']").val(),
                            bankAccount: $("#insert-table input[name='bankAccount']").val()
                        },
                        success: function (data, dataStatus) {
                            $("#insertDialog").dialog("close");
                            $("<div>添加成功</div>").alert();//.fadeIn(1000,"swing",function(){
                            //$(this).fadeOut("slow");
                            //});
                        },
                        error: function () {
                            $(this).dialog("close");
                        }
                    });
                }
            },
            {
                text: "取消",
                click: function (e) {
                    $(this).dialog("close");
                }
            }
        ]);
        $("#insertDialog").dialog("open");
    }

    function showUpdateDialog(id, name, shortName, address, post, phone, email, fex, clientPhone, clientName, bank, bankAccount) {
        $("#insert-table input[name='name']").val(name);
        $("#insert-table input[name='shortName']").val(shortName);
        $("#insert-table input[name='address']").val(address);
        $("#insert-table input[name='post']").val(post);
        $("#insert-table input[name='phone']").val(phone);
        $("#insert-table input[name='fex']").val(fex);
        $("#insert-table input[name='email']").val(email);
        $("#insert-table input[name='clientName']").val(clientName);
        $("#insert-table input[name='clientPhone']").val(clientPhone);
        $("#insert-table input[name='bank']").val(bank);
        $("#insert-table input[name='bankAccount']").val(bankAccount);
        $("#insertDialog").dialog("option", "buttons", [
            {
                text: "更新",
                click: function () {
                    $.ajax({
                        url: "${contextPath}/fim/cim?method=update",
                        method: "post",
                        dataType: "json",
                        data: {
                            id: id,
                            name: $("#insert-table input[name='name']").val(),
                            shortName: $("#insert-table input[name='shortName']").val(),
                            address: $("#insert-table input[name='address']").val(),
                            post: $("#insert-table input[name='post']").val(),
                            phone: $("#insert-table input[name='phone']").val(),
                            fex: $("#insert-table input[name='fex']").val(),
                            email: $("#insert-table input[name='email']").val(),
                            clientName: $("#insert-table input[name='clientName']").val(),
                            clientPhone: $("#insert-table input[name='clientPhone']").val(),
                            bank: $("#insert-table input[name='bank']").val(),
                            bankAccount: $("#insert-table input[name='bankAccount']").val()
                        },
                        success: function (data, dataStatus) {
                            $("#insertDialog").dialog("close");
                            $("<div>更新成功</div>").alert();//.fadeIn(1000,"swing",function(){
                            //$(this).fadeOut("slow");
                            //});
                        },
                        error: function () {
                            $(this).dialog("close");
                        }
                    });
                }
            },
            {
                text: "取消",
                click: function (e) {
                    $(this).dialog("close");
                }
            }
        ]);
        $("#insertDialog").dialog("open");
    }

    function deleteItem(id) {
        $.ajax({
            url: "${contextPath}/fim/cim?method=delete",
            method: "post",
            dataType: "json",
            data: {
                id: id
            },
            success: function (data, dataStatus) {
                if (data.success) {
                    $("<div>删除成功</div>").alert();//.fadeIn(1000,"swing",function(){
                    //$(this).fadeOut("slow");
                    //});
                    location.reload();
                }

            },
            error: function () {
                $("<div>删除失败</div>").alert();
            }
        });
    }
</script>