<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: coliza
  Date: 2014/5/28
  Time: 22:20
--%>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<div class="nav-bar">
    <ol class="breadcrumb">
        <li class="breadcrumb-head"><a href="#">首页</a></li>
        <li class="breadcrumb-item"><a href="#">库存管理</a></li>
        <li class="breadcrumb-item active"><a href="#">入库记录信息管理</a></li>
    </ol>
</div>
<div class="query-pane"></div>
<div class="records table-responsive">
    <div class="toolbar" align="right">
        <div class="insert-btn btn btn-primary" id="insert_import_btn">出入库</div>
    </div>
    <table class="table table-striped table-bordered table-hover table-condensed" width="800">
        <thead>
        <tr>
            <th width="12%" align="center">序号</th>
            <th width="12%" align="center">商品名</th>
            <th width="12%" align="center">数量</th>
            <th width="12%" align="center">单价</th>
            <th width="12%" align="center">应付款</th>
            <th width="12%" align="center">已付款</th>
            <th width="12%" align="center">操作员</th>
            <th width="12%" align="center">经手人</th>
            <th width="12%" align="center">结算方式</th>
            <th width="12%" align="center">入库类型</th>
            <th width="12%" align="center">入库日期</th>
            <th width="12%" align="center">操作</th>
        </tr>
        </thead>
        <tbody class="table-hover">
        <c:if test="${!empty records}">
            <c:forEach items="${records}" var="item" varStatus="index">
                <c:if test="${index.index%2 eq 0}">
                    <tr class="odd">
                        <td>${index.index+1}</td>
                        <td>${item.goodsInfo.name}</td>
                        <td>${item.amount}</td>
                        <td>${item.price}</td>
                        <td>${item.payment}</td>
                        <td>${item.paymentMade}</td>
                        <td>${item.operatorId}</td>
                        <td>${item.handlerId}</td>
                        <td>${item.payType}</td>
                        <td>${item.type}</td>
                        <td>${item.date}</td>
                        <td>
                            <a href="#" onclick="">删除</a>&nbsp;
                        </td>
                    </tr>
                </c:if>
                <c:if test="${index.index%2 != 0}">
                    <tr class="even">
                        <td>${index.index+1}</td>
                        <td>${item.goodsInfo.name}</td>
                        <td>${item.amount}</td>
                        <td>${item.price}</td>
                        <td>${item.payment}</td>
                        <td>${item.paymentMade}</td>
                        <td>${item.operatorId}</td>
                        <td>${item.handlerId}</td>
                        <td>${item.payType}</td>
                        <td>${item.type}</td>
                        <td>${item.date}</td>
                        <td>
                            <a href="#" onclick="">删除</a>&nbsp;
                        </td>
                    </tr>
                </c:if>
            </c:forEach>
        </c:if>
        </tbody>
    </table>
</div>