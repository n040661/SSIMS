<%--
  Created by IntelliJ IDEA.
  User: coliza
  Date: 2014/5/8
  Time: 21:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<div class="content">
    <div class="nav-bar">
        <ol class="breadcrumb">
            <li class="breadcrumb-head"><a href="#">首页</a></li>
            <li class="breadcrumb-item"><a href="#">基础信息管理</a></li>
            <li class="breadcrumb-item active"><a href="#">供应商信息管理</a></li>
        </ol>
    </div>
    <div class="query-pane"></div>
    <div class="records table-responsive">
        <div class="toolbar" align="right">
            <div class="insert-btn btn btn-primary" id="insert-btn">添加信息</div>
        </div>

        <table class="table table-hover table-condensed" width="800">
            <thead>
            <tr>
                <th align="center">供应商</th>
                <th align="center">简称</th>
                <th align="center">地址</th>
                <th align="center">邮编</th>
                <th align="center">电话</th>
                <th align="center">传真</th>
                <th align="center">联系人</th>
                <th align="center">联系人电话</th>
                <th align="center">电子邮件</th>
                <th width="10%" align="center">操作</th>
            </tr>
            </thead>
            <tbody>
            <c:if test="${0< records.size()}">
                <c:forEach items="${records}" var="item" varStatus="index">
                    <tr>
                        <td>${item.name}</td>
                        <td>${item.shortName}</td>
                        <td>${item.address}</td>
                        <td>${item.post}</td>
                        <td>${item.phone}</td>
                        <td>${item.fex}</td>
                        <td>${item.clientName}</td>
                        <td>${item.clientPhone}</td>
                        <td>${item.email}</td>
                        <td align="center">
                            <a href="javascript:void(0)"
                               onclick="showUpdateDialog('${item.id}','${item.name}','${item.shortName}','${item.address}','${item.post}','${item.phone}','${item.email}','${item.fex}','${item.clientName}','${item.clientPhone}')">修改</a>&nbsp;
                            <a href="javascript:void(0)" onclick="deleteItem('${item.id}')">删除</a>
                        </td>
                    </tr>
                </c:forEach>
            </c:if>
            </tbody>
        </table>
    </div>
</div>
<!-- Start 发布对话框 -->
<div id="insertDialog">
    <table id="insert-table" cellpadding="0" cellspacing="0">
        <tbody>
        <tr>
            <td align="right" width="20%"><label>全称:</label></td>
            <td width="30%"><input type="input" class="dialog-input-name form-control" name="name"/></td>
            <td align="right" width="20%"><label>简称:</label></td>
            <td width="30%"><input type="input" class="dialog-input-shortname form-control" name="shortName"/></td>
        </tr>
        <tr>
            <td align="right"><label>产地:</label></td>
            <td><input type="input" class="dialog-input-address form-control" name="origin"/></td>
            <td align="right"><label>批号:</label></td>
            <td><input type="input" class="dialog-input-post form-control" name="serialNo"/></td>
        </tr>
        <tr>
            <td align="right"><label>规格:</label></td>
            <td><input type="input" class="dialog-input-post form-control" name="specification"/></td>
            <td align="right"><label>包装:</label></td>
            <td><input type="input" class="dialog-input-phone form-control" name="pack"/></td>
        </tr>
        <tr>
            <td align="right"><label>计量单位:</label></td>
            <td><input type="input" class="dialog-input-fex form-control" name="unit"/></td>
            <td align="right"><label>批准文号:</label></td>
            <td><input type="input" class="dialog-input-email form-control" name="licence_no"/></td>
        </tr>
        <tr>
            <td align="right"><label>供应商全称:</label></td>
            <td><input type="input" class="dialog-input-client-n form-control" name="proSupplier"/></td>
            <td align="right"><label>备注:</label></td>
            <td><input type="input" class="dialog-input-client-p form-control" name="comment"/></td>
        </tr>
        </tbody>
    </table>
</div>
<!-- 脚本 -->
<script type="text/javascript">
    $(function () {
        $("#insertDialog").dialog({
            autoOpen: false,
            title: "添加商品信息",
            modal: true,
            width: 600,
            height: 350,
            resiable: false,
            dragable: false
        });
        $("#insert-btn").click(showInsertDialog);
    });
    function showInsertDialog() {
        $("#insertDialog").dialog("option", "buttons", [
            {
                text: "添加",
                click: function () {
                    $.ajax({
                        url: "${contextPath}/fim/gim?method=insert",
                        method: "post",
                        dataType: "json",
                        data: {
                            name: $("#insert-table input[name='name']").val(),
                            shortName: $("#insert-table input[name='shortName']").val(),
                            origin: $("#insert-table input[name='origin']").val(),
                            serialNo: $("#insert-table input[name='serialNo']").val(),
                            specification: $("#insert-table input[name='specification']").val(),
                            pack: $("#insert-table input[name='pack']").val(),
                            unit: $("#insert-table input[name='unit']").val(),
                            licenseNo: $("#insert-table input[name='licenseNo']").val(),
                            proSupplier: $("#insert-table input[name='proSupplier']").val(),
                            comment: $("#insert-table input[name='comment']").val()
                        },
                        success: function (data, dataStatus) {
                            $("#insertDialog").dialog("close");
                            $("<div>添加成功</div>").alert();//.fadeIn(1000,"swing",function(){
                            //$(this).fadeOut("slow");
                            //});
                        },
                        error: function () {
                            $(this).dialog("close");
                        }
                    });
                }
            },
            {
                text: "取消",
                click: function (e) {
                    $(this).dialog("close");
                }
            }
        ]);
        $("#insertDialog").dialog("open");
    }

    function showUpdateDialog(id, name, shortName, address, post, phone, email, fex, clientPhone, clientName, bank, bankAccount) {
        $("#insert-table input[name='name']").val(name);
        $("#insert-table input[name='shortName']").val(shortName);
        $("#insert-table input[name='origin']").val(address);
        $("#insert-table input[name='serialNo']").val(post);
        $("#insert-table input[name='specification']").val(phone);
        $("#insert-table input[name='pack']").val(fex);
        $("#insert-table input[name='unit']").val(email);
        $("#insert-table input[name='licenseNo']").val(clientName);
        $("#insert-table input[name='proSupplier']").val(clientPhone);
        $("#insert-table input[name='comment']").val(bank);
        $("#insertDialog").dialog("option", "buttons", [
            {
                text: "更新",
                click: function () {
                    $.ajax({
                        url: "${contextPath}/fim/gim?method=update",
                        method: "post",
                        dataType: "json",
                        data: {
                            id: id,
                            name: $("#insert-table input[name='name']").val(),
                            shortName: $("#insert-table input[name='shortName']").val(),
                            origin: $("#insert-table input[name='origin']").val(),
                            serialNo: $("#insert-table input[name='serialNo']").val(),
                            specification: $("#insert-table input[name='specification']").val(),
                            pack: $("#insert-table input[name='pack']").val(),
                            unit: $("#insert-table input[name='unit']").val(),
                            licenseNo: $("#insert-table input[name='licenseNo']").val(),
                            proSupplier: $("#insert-table input[name='proSupplier']").val(),
                            comment: $("#insert-table input[name='comment']").val()
                        },
                        success: function (data, dataStatus) {
                            $("#insertDialog").dialog("close");
                            $("<div>更新成功</div>").alert();//.fadeIn(1000,"swing",function(){
                            //$(this).fadeOut("slow");
                            //});
                        },
                        error: function () {
                            $(this).dialog("close");
                        }
                    });
                }
            },
            {
                text: "取消",
                click: function (e) {
                    $(this).dialog("close");
                }
            }
        ]);
        $("#insertDialog").dialog("open");
    }

    function deleteItem(id) {
        $.ajax({
            url: "${contextPath}/fim/cim?method=delete",
            method: "post",
            dataType: "json",
            data: {
                id: id
            },
            success: function (data, dataStatus) {
                if (data.success) {
                    $("<div>删除成功</div>").alert();//.fadeIn(1000,"swing",function(){
                    //$(this).fadeOut("slow");
                    //});
                    location.reload();
                }

            },
            error: function () {
                $("<div>删除失败</div>").alert();
            }
        });
    }
</script>