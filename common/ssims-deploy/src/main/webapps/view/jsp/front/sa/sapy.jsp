<%--
  Created by IntelliJ IDEA.
  User: coliza
  Date: 2014/6/2
  Time: 23:06
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<script src="${contextPath}/resources/scripts/echarts-all.js"></script>
<script type="text/javascript">
    $(function () {

        // 基于准备好的dom，初始化echarts图表
        var myChart = echarts.init(document.getElementById('sale_chart'));
        $.ajax({
            url: "${contextPath}/front/sa/sapy?method=getSalesGraphRest",
            dataType: "json",
            success: function (data) {
                if (data) {
                    eval(data);
                    // 为echarts对象加载数据
                    myChart.setOption(option);
                }

            },
            error: function () {
                alert("获取图表数据失败！");
            }
        });
    });
</script>
<div id="sale_chart" style="height:400px;width:100%"></div>
