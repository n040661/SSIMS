<%--
  Created by IntelliJ IDEA.
  User: coliza
  Date: 2014/6/2
  Time: 23:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<div class="nav-bar">
    <ol class="breadcrumb">
        <li class="breadcrumb-head"><a href="#">系统管理</a></li>
        <li class="breadcrumb-item active"><a href="#">用户管理</a></li>
    </ol>
</div>
<div class="query-pane"></div>
<div class="records table-responsive">
    <div class="toolbar" align="right">
        <div class="insert-btn btn btn-primary" id="insert_import_btn">添加用户</div>
    </div>
    <table class="table table-striped table-bordered table-hover table-condensed" width="800">
        <thead>
        <tr>
            <th width="5%" align="center">编号</th>
            <th width="12%" align="center">用户名</th>
            <th width="12%" align="center">角色</th>
            <th width="10%" align="center">操作</th>
        </tr>
        </thead>
        <tbody class="table-hover">
        <c:if test="${!empty pager.data}">
            <c:forEach items="${pager.data}" var="item" varStatus="index">
                <c:if test="${index.index%2 eq 0}">
                    <tr class="odd">
                        <td>${index.index+1}</td>
                        <td>${item.username}</td>
                        <td>
                            <c:forEach items="${item.roles}" var="role" varStatus="roleIndex">
                                <c:if test="${roleIndex.index eq 0}">
                                    ${role.role}
                                </c:if>
                                <c:if test="${roleIndex.index != 0}">
                                    ,${role.role}
                                </c:if>
                            </c:forEach>
                        </td>
                        <td align="center" valign="center">
                            <a href="#" onclick="deleteItem('${item.id}')">删除</a>&nbsp;
                        </td>
                    </tr>
                </c:if>
                <c:if test="${index.index%2 != 0}">
                    <tr class="even">
                        <td>${index.index+1}</td>
                        <td>${item.username}</td>
                        <td>
                            <c:forEach items="${item.roles}" var="role" varStatus="roleIndex">
                                <c:if test="${roleIndex.index eq 0}">
                                    ${role.role}
                                </c:if>
                                <c:if test="${roleIndex.index != 0}">
                                    ,${role.role}
                                </c:if>
                            </c:forEach>
                        </td>
                        <td align="center" valign="center">
                            <a href="#" onclick="deleteItem('${item.id}')">删除</a>&nbsp;
                        </td>
                    </tr>
                </c:if>
            </c:forEach>
        </c:if>
        </tbody>
    </table>
</div>

<!-- Start 添加销售记录对话框 -->
<!--
<div id="insertDialog" >
<table id="insert-table" cellpadding="0" cellspacing="0">
<tbody>
<tr>
<td align="right"><label>销售票号:</label></td><td colspan="3"><input type="input" class="dialog-input-client-n form-control" name="saleNo"/></td>
</tr>
<tr>
<td align="right" width="20%"><label>客户:</label></td><td width="30%">

</td>
<td align="right" width="20%"><label>商品:</label></td><td width="30%"><select name="goodsId">

</td>
</tr>
<tr>
<td align="right"><label>单价:</label></td><td><input type="input" class="dialog-input-address form-control" name="price" /></td>
<td align="right"><label>数量:</label></td><td><input type="input" class="dialog-input-address form-control" name="amount" /></td>
</tr>
<tr>
<td align="right"><label>实收款:</label></td><td><input type="input" class="dialog-input-post form-control" name="paymentMade"/></td>
<td align="right"><label>经手人:</label></td><td><input type="input" class="dialog-input-phone form-control" name="handlerId"/></td>
</tr>
<tr>
<td align="right"><label>操作员:</label></td><td><input type="input" class="dialog-input-fex form-control" name="operatorId"/></td>
<td align="right"><label>销售日期:</label></td><td><input type="input" class="dialog-input-email form-control" name="date"/></td>
</tr>
<tr>
<td align="right"><label>结算方式:</label></td><td>
<select name="payType">
<option value="0">现金</option>
<option value="1">预付</option>
<option value="2">支票</option>
<option value="3">挂账</option>
</select>
</td>
<td align="right"><label>类型:</label></td><td>
<select name="type">
<option value="0">销售</option>
<option value="1">退货</option>
</select></td>
</tr>
</tbody>
</table>
</div>
-->
<!-- 脚本 -->
<!--
<script type="text/javascript">
var insertDialog;
$(function(){
insertDialog =$("#insertDialog").dialog({
autoOpen:false,
title:"添加销售记录",
modal:true,
width:600,
height:400,
resiable:false,
dragable:false
});
$("#insert_import_btn").click(showInsertDialog);
});
function showInsertDialog(){
$("#insertDialog").dialog("option","buttons",[
{
text:"添加",
click:function(){
$.ajax({
url:"${contextPath}/gs/sm?method=insert",
type:"post",
dataType:"json",
data:{
"saleNo":$("#insert-table input[name='saleNo']").val(),
"customerId":$("#insert-table select[name='customerId']").val(),
"goodsId":$("#insert-table select[name='goodsId']").val(),
"price":$("#insert-table input[name='price']").val(),
"amount":$("#insert-table input[name='amount']").val(),
"paymentMade":$("#insert-table input[name='paymentMade']").val(),
"handlerId":$("#insert-table input[name='handlerId']").val(),
"operatorId":$("#insert-table input[name='operatorId']").val(),
"date":$("#insert-table input[name='date']").val(),
"payType":$("#insert-table select[name='payType']").val(),
"type":$("#insert-table select[name='type']").val()
},
success:function(data,dataStatus){
if(insertDialog) {
insertDialog.dialog("close");
}
//.fadeIn(1000,"swing",function(){
//$(this).fadeOut("slow");
//});
},
error:function(e){
//$(this).dialog("close");
}
});
}
},
{
text:"取消",
click:function(e){
$(this).dialog("close");
}
}
]);
if(insertDialog){
insertDialog.dialog("open");
}
}
function deleteItem(id){
$.ajax({
url:"${contextPath}/gs/sm?method=delete",
method:"post",
dataType:"json",
data:{
id:id
},
success:function(data,dataStatus){
if(data.success){
$("<div>删除成功</div>").alert();//.fadeIn(1000,"swing",function(){
//$(this).fadeOut("slow");
//});
location.reload();
}

},
error:function(){
$("<div>删除失败</div>").alert();
}
});
}
</script>
-->