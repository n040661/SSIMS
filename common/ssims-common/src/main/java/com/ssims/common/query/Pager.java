package com.ssims.common.query;

import java.util.List;
import java.util.Map;

/**
 * Created by coliza on 2017/12/22.
 */
public class Pager<T> {
    public final static String PAGE_NO_KEY = "pageNo";
    public final static String PAGE_SIZE_KEY = "pageSize";
    public final static int DEFAULT_PAGE_SIZE = 10;
    public final static int DEFAULT_PAGE_NO = 1;

    private Integer pageNo;
    private Integer pageSize;
    private Integer totalAmount;
    private List<T> data;

    public Integer getPageNo() {
        if(pageNo == null)return DEFAULT_PAGE_NO;
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public Integer getPageSize() {
        if(pageSize == null) return DEFAULT_PAGE_SIZE;
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getTotalAmount() {
        if(data == null){
            return 0;
        }
        return totalAmount;
    }

    public void setTotalAmount(int totalAmount) {
        this.totalAmount = totalAmount;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    private Pager fillPagerParam(Map params){
        Object pageNoObj = params.get(PAGE_NO_KEY);
        if(pageNoObj != null) {
            setPageNo((int) pageNoObj);
        }
        Object pageSizeObj = params.get(PAGE_SIZE_KEY);
        if(pageSizeObj != null) {
            setPageNo((int) pageSizeObj);
        }
        return this;
    }
    public static int getStart(){
        return 0;
    }
    public static int getStart(int pageNo, int pageSize){
        return (pageNo-1)*pageSize;
    }
    public static int getStart(Map params){
        Object pageNoObj = params.get(PAGE_NO_KEY);
        if(pageNoObj == null) return 0;
        int pageNo = (int)pageNoObj;
        Object pageSizeObj = params.get(PAGE_SIZE_KEY);
        int pageSize = DEFAULT_PAGE_SIZE;
        if(pageSizeObj != null) {
            pageSize = (int)pageSizeObj;
        }
        return getStart(pageNo, pageSize);
    }
    public static int getOffset(){
        return DEFAULT_PAGE_SIZE;
    }
    public static int getOffset(int pageSize){
        return pageSize;
    }

    public static int getOffset(Map params){
        Object pageSizeObj = params.get(PAGE_SIZE_KEY);
        int pageSize = DEFAULT_PAGE_SIZE;
        if(pageSizeObj != null) {
            pageSize = (int)pageSizeObj;
        }
        return getOffset(pageSize);
    }

    public static Map decorateMap(Map params){
        Object pageNoObj = params.get(PAGE_NO_KEY);
        if(pageNoObj == null) {
            params.put(PAGE_NO_KEY,DEFAULT_PAGE_NO);
        }
        Object pageSizeObj = params.get(PAGE_SIZE_KEY);
        if(pageSizeObj == null) {
            params.put(PAGE_SIZE_KEY,DEFAULT_PAGE_SIZE);
        }
        params.put("start", getStart(params));
        params.put("offset", getOffset(params));
        return params;
    }
    public static <T> Pager<T> from(Map params, List<T> data){
        Pager pager = new Pager();
        pager.fillPagerParam(params);
        pager.setData(data);
        return pager;
    }
}
