package com.ssims.common.util;

import java.util.UUID;

/**
 * Created by coliza on 2015/8/23.
 */
public class FrontUtils {
    public static String generateUUID() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString();
    }
}
