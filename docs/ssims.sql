﻿# Host: 59.110.7.93  (Version 5.6.33)
# Date: 2017-12-22 01:49:54
# Generator: MySQL-Front 6.0  (Build 2.20)


#
# Structure for table "ss_customer"
#

DROP TABLE IF EXISTS `ss_customer`;
CREATE TABLE `ss_customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '名称',
  `short_name` varchar(255) DEFAULT NULL COMMENT '简称',
  `address` varchar(255) DEFAULT NULL COMMENT '地址',
  `post` varchar(255) DEFAULT NULL COMMENT '邮编',
  `email` varchar(255) DEFAULT NULL COMMENT '电子邮件',
  `fex` varchar(255) DEFAULT NULL COMMENT '传真',
  `phone` varchar(255) DEFAULT NULL COMMENT '联系电话',
  `client_name` varchar(255) DEFAULT NULL COMMENT '联系人姓名',
  `client_phone` varchar(255) DEFAULT NULL COMMENT '联系人电话',
  `bank` varchar(255) DEFAULT NULL COMMENT '开户银行',
  `bank_account` varchar(255) DEFAULT NULL COMMENT '银行账户',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='客户信息表';

#
# Data for table "ss_customer"
#

INSERT INTO `ss_customer` VALUES (1,'新东方','xdf','成都市东大街阿斯顿撒旦撒旦是倒萨倒萨打算到三点','11','11','11','11','11','12323','11','12');

#
# Structure for table "ss_goods"
#

DROP TABLE IF EXISTS `ss_goods`;
CREATE TABLE `ss_goods` (
  `id` varchar(255) NOT NULL DEFAULT '' COMMENT '主键',
  `name` varchar(255) NOT NULL DEFAULT '商品' COMMENT '商品名称',
  `short_name` varchar(255) NOT NULL DEFAULT '商品' COMMENT '简称',
  `origin` varchar(255) DEFAULT NULL COMMENT '产地',
  `serial_no` varchar(255) DEFAULT NULL COMMENT '批准文号',
  `specification` varchar(255) DEFAULT NULL COMMENT '规格',
  `pack` varchar(255) DEFAULT NULL COMMENT '包装',
  `unit` varchar(255) DEFAULT NULL COMMENT '计量单位',
  `license_no` varchar(255) DEFAULT NULL COMMENT '批准文号',
  `pro_supplier` varchar(255) DEFAULT NULL COMMENT '供应商',
  `comment` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品信息表';

#
# Data for table "ss_goods"
#

INSERT INTO `ss_goods` VALUES ('G000000001','sony','sony','江西','DASK9913','15*14CM','盒子','cm',NULL,NULL,'医药');

#
# Structure for table "ss_goods_sale"
#

DROP TABLE IF EXISTS `ss_goods_sale`;
CREATE TABLE `ss_goods_sale` (
  `id` varchar(255) NOT NULL DEFAULT '' COMMENT '主键',
  `client_name` varchar(255) DEFAULT NULL COMMENT '客户全称',
  `goods_id` varchar(255) NOT NULL DEFAULT '' COMMENT '商品编号',
  `price` double(12,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '销售单价',
  `money` double(12,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '销售金额',
  `amount` bigint(20) unsigned NOT NULL DEFAULT '1' COMMENT '销售数量',
  `payment` double(12,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '应收款',
  `payment_made` double unsigned NOT NULL DEFAULT '0' COMMENT '实收款',
  `payment_due` double(12,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '未付款',
  `operator_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '操作员ID',
  `handler_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '经手人ID',
  `date` datetime NOT NULL COMMENT '销售日期',
  `pey_type` varchar(255) NOT NULL DEFAULT '现金' COMMENT '结算方式（现金、预付、支票、挂账）',
  `type` varchar(255) NOT NULL DEFAULT '销售' COMMENT '销售类型（退货、销售）',
  `sale_no` varchar(255) NOT NULL DEFAULT '0000000001' COMMENT '销售票号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品销售信息表';

#
# Data for table "ss_goods_sale"
#


#
# Structure for table "ss_permission"
#

DROP TABLE IF EXISTS `ss_permission`;
CREATE TABLE `ss_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permission` varchar(255) DEFAULT NULL COMMENT '权限名称',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='权限表';

#
# Data for table "ss_permission"
#


#
# Structure for table "ss_role"
#

DROP TABLE IF EXISTS `ss_role`;
CREATE TABLE `ss_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(255) DEFAULT NULL COMMENT '角色名称',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `available` bit(1) DEFAULT NULL COMMENT '是否可用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色表';

#
# Data for table "ss_role"
#

INSERT INTO `ss_role` VALUES (1,'ADMIN','管理员',b'1');

#
# Structure for table "ss_role_permission"
#

DROP TABLE IF EXISTS `ss_role_permission`;
CREATE TABLE `ss_role_permission` (
  `permission_id` int(11) NOT NULL DEFAULT '0' COMMENT '权限ID',
  `role_id` int(11) NOT NULL DEFAULT '0' COMMENT '角色ID',
  PRIMARY KEY (`permission_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色权限映射表';

#
# Data for table "ss_role_permission"
#


#
# Structure for table "ss_sale_check"
#

DROP TABLE IF EXISTS `ss_sale_check`;
CREATE TABLE `ss_sale_check` (
  `id` varchar(255) NOT NULL DEFAULT '',
  `sale_no` varchar(255) DEFAULT NULL COMMENT '销售票号',
  `check_no` varchar(255) DEFAULT NULL COMMENT '结账票号',
  `customer` varchar(255) DEFAULT NULL COMMENT '客户名称',
  `check_amount` double(12,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '本次结账',
  `due_amount` double(12,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '未付金额',
  `create_date` datetime NOT NULL COMMENT '结账日期',
  `handler_id` int(11) unsigned DEFAULT NULL COMMENT '经手人id',
  `operator_id` int(11) unsigned DEFAULT NULL COMMENT '操作员id',
  `type` varchar(255) NOT NULL DEFAULT '销售' COMMENT '销售类型（销售、退货）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品销售结账记录表';

#
# Data for table "ss_sale_check"
#


#
# Structure for table "ss_storage"
#

DROP TABLE IF EXISTS `ss_storage`;
CREATE TABLE `ss_storage` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(255) DEFAULT NULL COMMENT '仓库名称',
  `info` text COMMENT '仓库说明',
  `address` text COMMENT '详细地址（社区，楼，单元，层，室）',
  `city_id` int(11) NOT NULL DEFAULT '0' COMMENT '城市ID',
  `province_id` int(11) NOT NULL DEFAULT '0' COMMENT '省份ID',
  `district_id` int(11) NOT NULL DEFAULT '0' COMMENT '区县ID',
  `area` double(8,2) NOT NULL DEFAULT '0.00' COMMENT '占地面积（平方米）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='仓库信息表';

#
# Data for table "ss_storage"
#


#
# Structure for table "ss_supplier"
#

DROP TABLE IF EXISTS `ss_supplier`;
CREATE TABLE `ss_supplier` (
  `id` varchar(255) NOT NULL DEFAULT '' COMMENT '主键',
  `name` varchar(255) NOT NULL DEFAULT '供应商' COMMENT '供应商全称',
  `short_name` varchar(255) DEFAULT NULL COMMENT '简称',
  `address` varchar(255) DEFAULT NULL COMMENT '地址',
  `post` varchar(255) DEFAULT NULL COMMENT '邮编',
  `phone` varchar(255) DEFAULT NULL COMMENT '电话',
  `fex` varchar(255) DEFAULT NULL COMMENT '传真',
  `client_name` varchar(255) DEFAULT NULL COMMENT '联系人',
  `client_phone` varchar(255) DEFAULT NULL COMMENT '联系人电话',
  `email` varchar(255) DEFAULT NULL COMMENT '电子邮件',
  `bank` varchar(255) DEFAULT NULL COMMENT '开户银行',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='供应商信息';

#
# Data for table "ss_supplier"
#


#
# Structure for table "ss_user"
#

DROP TABLE IF EXISTS `ss_user`;
CREATE TABLE `ss_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `username` varchar(255) NOT NULL DEFAULT 'user' COMMENT '用户名',
  `password` varchar(255) NOT NULL DEFAULT '123456' COMMENT '密码',
  `locked` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否被锁',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='用户表';

#
# Data for table "ss_user"
#

INSERT INTO `ss_user` VALUES (1,'user','3132333435366e756c6c',b'0');

#
# Structure for table "ss_user_role"
#

DROP TABLE IF EXISTS `ss_user_role`;
CREATE TABLE `ss_user_role` (
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户ID',
  `role_id` int(11) NOT NULL DEFAULT '0' COMMENT '角色ID',
  PRIMARY KEY (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='账户角色关系映射表';

#
# Data for table "ss_user_role"
#

INSERT INTO `ss_user_role` VALUES (1,1);

#
# Structure for table "ss_wh"
#

DROP TABLE IF EXISTS `ss_wh`;
CREATE TABLE `ss_wh` (
  `id` varchar(255) NOT NULL DEFAULT '' COMMENT '主键',
  `goods_id` varchar(255) NOT NULL DEFAULT '0' COMMENT '商品编号',
  `amount` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '数量',
  `storage_id` varchar(11) NOT NULL DEFAULT '' COMMENT '仓库ID',
  PRIMARY KEY (`id`),
  UNIQUE KEY `goods` (`goods_id`) COMMENT '商品编号索引'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='库存信息';

#
# Data for table "ss_wh"
#

INSERT INTO `ss_wh` VALUES ('WH0','G000000001',10,'');

#
# Structure for table "ss_wh_check"
#

DROP TABLE IF EXISTS `ss_wh_check`;
CREATE TABLE `ss_wh_check` (
  `id` varchar(11) NOT NULL DEFAULT '',
  `check_no` varchar(255) DEFAULT NULL COMMENT '结账票号',
  `sale_no` varchar(255) DEFAULT NULL COMMENT '销售票号',
  `customer` varchar(255) DEFAULT NULL COMMENT '客户名称',
  `check_amount` double(12,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '本次结账',
  `due_amount` double(12,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '未付金额',
  `create_date` datetime DEFAULT NULL COMMENT '结款日期',
  `operator_id` int(11) unsigned DEFAULT NULL COMMENT '操作员ID',
  `handler_id` int(11) unsigned DEFAULT NULL COMMENT '经手人ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品入库结账记录表';

#
# Data for table "ss_wh_check"
#


#
# Structure for table "ss_wh_inout"
#

DROP TABLE IF EXISTS `ss_wh_inout`;
CREATE TABLE `ss_wh_inout` (
  `id` varchar(255) NOT NULL DEFAULT '' COMMENT '主键',
  `goods_id` varchar(255) NOT NULL DEFAULT '0' COMMENT '商品编号',
  `amount` bigint(20) NOT NULL DEFAULT '0' COMMENT '数量',
  `price` double(12,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '单价',
  `payment_due` double(12,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '未付款',
  `payment` double(12,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '应付款',
  `payment_made` double(12,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '已付款',
  `operator_id` varchar(255) NOT NULL DEFAULT '0' COMMENT '操作员编号',
  `handler_id` varchar(255) NOT NULL DEFAULT '0' COMMENT '经手人ID',
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '日期',
  `pay_type` varchar(255) NOT NULL DEFAULT '现金' COMMENT '结算方式（现金，预付，支票，挂账）',
  `type` varchar(255) DEFAULT '新进' COMMENT '入库类型（0：新进入库,1：退货入库,2：出库）',
  `wh_no` varchar(255) NOT NULL DEFAULT '' COMMENT '票号',
  `storage_id` int(11) DEFAULT NULL COMMENT '仓库号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品入库记录表';

#
# Data for table "ss_wh_inout"
#

INSERT INTO `ss_wh_inout` VALUES ('WH0','G000000001',10,100.00,900.00,1000.00,100.00,'a397a25553be41fc996b521413e9e22d','a397a25553be41fc996b521413e9e22d','2015-06-03 23:29:41','0','0','WHI1433345381866',NULL);
