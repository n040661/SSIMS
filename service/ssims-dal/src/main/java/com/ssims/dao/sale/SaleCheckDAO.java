package com.ssims.dao.sale;

import com.ssims.dao.BaseDAO;
import com.ssims.model.entity.SaleCheckEntity;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by coliza on 2014/8/17.
 */
@Repository("saleCheckDAO")
public class SaleCheckDAO extends BaseDAO {
    public List<SaleCheckEntity> query() {
        return sqlSession.selectList("com.ssims.front.dao.sc.mapper.SaleCheckMapper.select");
    }
}
