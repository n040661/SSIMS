package com.ssims.dao.client;

import com.ssims.dao.BaseDAO;
import com.ssims.model.entity.GoodsInfoEntity;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by coliza on 2014/5/8.
 */
@Repository("goodsInfoDAO")
public class GoodsInfoDAO extends BaseDAO{
    public List<GoodsInfoEntity> query() {
        return sqlSession.selectList("GoodsInfoMapper.select");
    }

    public int insert(GoodsInfoEntity entity) {
        return sqlSession.insert("GoodsInfoMapper.insert", entity);
    }

    public boolean delete(String id) {
        int count = sqlSession.delete("GoodsInfoMapper.deleteById", id);
        if (count > 0) return true;
        return false;
    }

    public boolean update(GoodsInfoEntity entity) {
        int count = sqlSession.update("GoodsInfoMapper.update", entity);
        if (count > 0) return true;
        return false;
    }

    public GoodsInfoEntity queryById(String id) {
        return sqlSession.selectOne("GoodsInfoMapper.selectOneById", id);
    }
}
