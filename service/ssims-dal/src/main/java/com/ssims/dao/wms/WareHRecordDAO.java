package com.ssims.dao.wms;

import com.ssims.dao.BaseDAO;
import com.ssims.model.entity.WareHRecordEntity;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 入库记录DAO类
 * Created by coliza on 2014/8/17.
 */
@Repository("wareHRecordDAO")
public class WareHRecordDAO extends BaseDAO {

    public List<WareHRecordEntity> query() {
        return sqlSession.selectList("com.ssims.front.dao.ewh.mapper.WareHRecordMapper.select");
    }
}
