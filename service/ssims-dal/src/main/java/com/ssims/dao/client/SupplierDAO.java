package com.ssims.dao.client;

import com.ssims.dao.BaseDAO;
import com.ssims.model.entity.SupplierEntity;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by coliza on 2014/5/11.
 */
@Repository("supplierDAO")
public class SupplierDAO extends BaseDAO{
    public List<SupplierEntity> query() {
        return sqlSession.selectList("SupplierMapper.select");
    }

    public int insert(SupplierEntity entity) {
        return sqlSession.insert("SupplierMapper.insert", entity);
    }

    public boolean delete(Integer id) {
        int count = sqlSession.delete("SupplierMapper.deleteById", id);
        if (count > 0) return true;
        return false;
    }

    public boolean update(SupplierEntity entity) {
        int count = sqlSession.update("SupplierMapper.update", entity);
        if (count > 0) return true;
        return false;
    }

    public SupplierEntity queryById(Integer id) {
        return sqlSession.selectOne("com.ssims.front.dao.fim.mapper.SupplierInfoMapper.selectOneById", id);
    }
}
