package com.ssims.dao.system;

import com.ssims.dao.BaseDAO;
import com.ssims.security.entity.SecurityUser;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by coliza on 2014/6/10.
 */
@Repository("systemDAO")
public class SystemDAO extends BaseDAO {
    public List<SecurityUser> query() {
        return null;
    }

    public String add(SecurityUser entity) {
        return String.valueOf(sqlSession.insert("SystemMapper.insert"));
    }

    public boolean deleteById(String uid) {
        return sqlSession.delete("SystemMapper.delete", uid) > 0;
    }

    public boolean updatePassword(String uid) {
        return sqlSession.update("SystemMapper.updatePassword") > 0;
    }
}
