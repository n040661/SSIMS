package com.ssims.dao.wms;

import com.ssims.dao.BaseDAO;
import com.ssims.model.entity.GoodsStockInfoEntity;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by coliza on 2014/6/2.
 */
@Repository("goodsStockDAO")
public class GoodsStockDAO extends BaseDAO {

    public List<GoodsStockInfoEntity> query() {
        return sqlSession.selectList("GoodsStockMapper.select");
    }
}
