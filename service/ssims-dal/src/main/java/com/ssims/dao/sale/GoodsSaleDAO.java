package com.ssims.dao.sale;

import com.ssims.dao.BaseDAO;
import com.ssims.model.entity.GoodsSaleEntity;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by coliza on 2014/8/15.
 */
@Repository("goodsSaleDAO")
public class GoodsSaleDAO extends BaseDAO {
    public List<GoodsSaleEntity> query() {
        return sqlSession.selectList("GoodsSaleMapper.select");
    }
    public int insert(GoodsSaleEntity entity) {
        return sqlSession.insert("GoodsSaleMapper.insert", entity);
    }
    public boolean delete(String id) {
        int count = sqlSession.delete("GoodsSaleMapper.deleteById", id);
        if (count > 0) return true;
        return false;
    }

    public boolean update(GoodsSaleEntity entity) {
        int count = sqlSession.update("GoodsSaleMapper.update", entity);
        if (count > 0) return true;
        return false;
    }

    public GoodsSaleEntity queryById(String id) {
        return sqlSession.selectOne("GoodsSaleMapper.selectOneById", id);
    }
}
