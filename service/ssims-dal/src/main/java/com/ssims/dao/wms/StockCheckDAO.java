package com.ssims.dao.wms;

import com.ssims.dao.BaseDAO;
import com.ssims.model.entity.StockCheckEntity;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by coliza on 2014/8/18.
 */
@Repository("stockCheckDAO")
public class StockCheckDAO extends BaseDAO {
    public List<StockCheckEntity> query() {
        return sqlSession.selectList("com.ssims.front.dao.sc.mapper.StockCheckMapper.select");
    }
}
