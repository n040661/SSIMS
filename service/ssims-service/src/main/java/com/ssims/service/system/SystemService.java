package com.ssims.service.system;

import com.ssims.dao.system.SystemDAO;
import com.ssims.security.entity.SecurityUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by coliza on 2014/6/10.
 */
@Transactional
@Service("systemService")
public class SystemService{
    @Autowired
    private SystemDAO systemDAO;

    public List<SecurityUser> queryAllUser() {
        return systemDAO.query();
    }

    public String addUser(SecurityUser entity) {
        return null;
    }

    public boolean deleteUserById(String uid) {
        return false;
    }
}
