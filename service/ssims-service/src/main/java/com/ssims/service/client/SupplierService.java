package com.ssims.service.client;

import com.ssims.dao.client.SupplierDAO;
import com.ssims.model.entity.SupplierEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by coliza on 2014/5/11.
 */
@Transactional
@Service("supplierService")
public class SupplierService{
    private final static Logger log = LoggerFactory.getLogger(SupplierService.class);
    @Autowired
    private SupplierDAO supplierDAO;

    public List<SupplierEntity> queryByParams() {
        return supplierDAO.query();
    }

    public List<SupplierEntity> saveAndRefresh(SupplierEntity entity) {
        supplierDAO.insert(entity);
        return supplierDAO.query();
    }

    public SupplierEntity updateCusInfo(SupplierEntity entity) {
        boolean success = supplierDAO.update(entity);
        if (success) {
            return supplierDAO.queryById(entity.getId());
        }
        return null;
    }

    public boolean delete(Integer id) {
        return supplierDAO.delete(id);
    }
}
