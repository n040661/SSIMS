package com.ssims.service.sale;

import com.ssims.dao.wms.GoodsStockDAO;
import com.ssims.model.entity.GoodsStockInfoEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by coliza on 2014/6/2.
 */
@Transactional
@Service("goodsStockService")
public class GoodsStockService{

    @Autowired
    private GoodsStockDAO goodsStockDAO;

    public List<GoodsStockInfoEntity> queryByParams() {
        return goodsStockDAO.query();
    }

    public List<GoodsStockInfoEntity> saveAndRefresh(GoodsStockInfoEntity entity) {
        return null;
    }

    public GoodsStockInfoEntity updateGoodsStockInfo(GoodsStockInfoEntity entity) {
        return null;
    }

    public boolean delete(Integer id) {
        return false;
    }
}
