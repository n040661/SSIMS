package com.ssims.service.client;

import com.ssims.dao.client.CusInfoDAO;
import com.ssims.model.entity.CusInfoEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by coliza on 2014/5/7.
 */
@Transactional
@Service("cusInfoService")
public class CusInfoService{
    @Autowired
    private CusInfoDAO cusInfoDAO;

    public List<CusInfoEntity> queryByParams() {
        return cusInfoDAO.query();
    }

    public List<CusInfoEntity> saveAndRefresh(CusInfoEntity entity) {
        cusInfoDAO.insert(entity);
        return cusInfoDAO.query();
    }

    public CusInfoEntity updateCusInfo(CusInfoEntity entity) {
        boolean success = cusInfoDAO.update(entity);
        if (success) {
            return cusInfoDAO.queryById(entity.getId());
        }
        return null;
    }

    public boolean delete(Integer id) {
        return cusInfoDAO.delete(id);
    }
}
