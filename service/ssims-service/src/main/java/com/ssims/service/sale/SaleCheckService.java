package com.ssims.service.sale;

import com.ssims.dao.sale.SaleCheckDAO;
import com.ssims.model.entity.SaleCheckEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by coliza on 2014/8/18.
 */
@Transactional
@Service("saleCheckService")
public class SaleCheckService{

    @Autowired
    private SaleCheckDAO saleCheckDAO;

    public List<SaleCheckEntity> queryByParams() {
        return saleCheckDAO.query();
    }
}
