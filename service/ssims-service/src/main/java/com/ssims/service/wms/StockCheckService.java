package com.ssims.service.wms;

import com.ssims.dao.wms.StockCheckDAO;
import com.ssims.model.entity.StockCheckEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by coliza on 2014/8/18.
 */
@Transactional
@Service("stockCheckService")
public class StockCheckService{
    @Autowired
    private StockCheckDAO stockCheckDAO;

    public List<StockCheckEntity> queryByParams() {
        return stockCheckDAO.query();
    }
}
