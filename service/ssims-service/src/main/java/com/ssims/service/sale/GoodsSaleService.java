package com.ssims.service.sale;

import com.ssims.common.util.FrontUtils;
import com.ssims.dao.sale.GoodsSaleDAO;
import com.ssims.model.entity.GoodsSaleEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by coliza on 2014/8/15.
 */
@Transactional
@Service("goodsSaleService")
public class GoodsSaleService{
    @Autowired
    private GoodsSaleDAO goodsSaleDAO;

    public List<GoodsSaleEntity> queryByParams() {
        return goodsSaleDAO.query();
    }

    public int saveAndRefresh(GoodsSaleEntity entity) {
        //处理数据
        entity.setId(FrontUtils.generateUUID());
        double payment = entity.getPrice() * entity.getAmount();
        entity.setPayment(payment);
        entity.setPaymentDue(entity.getPayment() - entity.getPaymentMade());
        entity.setMoney(payment);
        return goodsSaleDAO.insert(entity);
    }

    public GoodsSaleEntity updateGoodsSale(GoodsSaleEntity entity) {
        return null;
    }

    public boolean delete(String id) {
        return goodsSaleDAO.delete(id);
    }
}
