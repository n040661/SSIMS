package com.ssims.service.wms;

import com.ssims.dao.wms.WareHRecordDAO;
import com.ssims.model.entity.WareHRecordEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 入库记录服务类
 * Created by coliza on 2014/8/17.
 */
@Transactional
@Service("wareHRecordService")
public class WareHRecordService{
    @Autowired
    private WareHRecordDAO wareHRecordDAO;

    public List<WareHRecordEntity> queryByParams() {
        return wareHRecordDAO.query();
    }
}
