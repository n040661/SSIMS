package com.ssims.controller.admin;

import com.ssims.common.query.Pager;
import com.ssims.controller.BaseController;
import com.ssims.security.entity.SecurityUser;
import com.ssims.security.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * @description 用户管理控制器，控制用户管理中的所有操作
 * @date 2015年9月17日
 */
@Controller("userAdminController")
@RequestMapping("/admin/user")
public class UserAdminController extends BaseController {

    @Autowired
    private UserService userService;

    @RequestMapping(params = "method=show")
    public ModelAndView showAllUsers(HttpServletRequest request, HttpServletResponse response) {
        Map params = new HashMap<String, Object>();
        Pager<SecurityUser> pager = userService.findAllUserExcludeAdmin(params);
        request.setAttribute("pager", pager);
        return adminLayout("admin/user/um", request, response);
    }
}
