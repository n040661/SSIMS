package com.ssims.controller.front;

import com.ssims.controller.BaseController;
import com.ssims.service.wms.WareHRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by coliza on 2014/8/17.
 */
@Controller("wareHRecordController")
@RequestMapping("/front/gsm/ewh")
public class WareHRecordManagerController extends BaseController {

    @Autowired
    private WareHRecordService wareHRecordService;

    @RequestMapping(params = "method=show")
    public ModelAndView show(HttpServletRequest request, HttpServletResponse response) {
        List list = wareHRecordService.queryByParams();
        request.setAttribute("records", list);
        return mainLayout("front/gsm/ewh", request, response);
    }
}
