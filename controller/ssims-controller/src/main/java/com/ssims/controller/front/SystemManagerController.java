package com.ssims.controller.front;

import com.ssims.controller.BaseController;
import com.ssims.security.entity.SecurityUser;
import com.ssims.service.system.SystemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by coliza on 2014/6/2.
 */
@Controller("systemManagerController")
@RequestMapping("/front/system")
public class SystemManagerController extends BaseController {

    @Autowired
    private SystemService systemService;

    @RequestMapping("/account/view")
    public ModelAndView accountManagement(HttpServletRequest request, HttpServletResponse response) {
        List<SecurityUser> list = systemService.queryAllUser();
        request.setAttribute("records", list);
        return mainLayout("front/system/manage", request, response);
    }

    @RequestMapping("/account/admin/password")
    public ModelAndView adminPassword(HttpServletRequest request, HttpServletResponse response) {

        return mainLayout("front/system/password", request, response);
    }
}
