package com.ssims.controller.front;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ssims.controller.BaseController;
import com.ssims.model.entity.SupplierEntity;
import com.ssims.service.client.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by coliza on 2014/5/11.
 */
@Controller("supplierManagerController")
@RequestMapping("/front/fim/sim")
public class SupplierManagerController extends BaseController {
    @Autowired
    private SupplierService supplierService;

    @RequestMapping(params = "method=show")
    public ModelAndView show(HttpServletRequest request, HttpServletResponse response) {
        List list = supplierService.queryByParams();
        request.setAttribute("records", list);
        return mainLayout("front/fim/sim", request, response);
    }

    @RequestMapping(params = "method=insert")
    public
    @ResponseBody
    String insert(HttpServletRequest request, HttpServletResponse response, SupplierEntity entity) {
        List list = supplierService.saveAndRefresh(entity);
        JSONArray listArray = new JSONArray();
        JSONObject obj = new JSONObject();
        if (null == list) {
            obj.put("items", null);
            return obj.toString();
        }
        listArray.add(obj);
        obj.put("items", listArray);
        return obj.toJSONString();

    }

    @RequestMapping(params = "method=update")
    public
    @ResponseBody
    String update(HttpServletRequest request, HttpServletResponse response, SupplierEntity entity) {
        SupplierEntity one = supplierService.updateCusInfo(entity);
        JSONArray listArray = new JSONArray();
        JSONObject obj = new JSONObject();
        if (null == one) {
            obj.put("items", null);
            return obj.toString();
        }
        listArray.add(one);
        obj.put("items", listArray);
        return obj.toJSONString();

    }

    @RequestMapping(params = "method=delete")
    public
    @ResponseBody
    String delete(HttpServletRequest request, HttpServletResponse response, @RequestParam("id") Integer id) {
        Boolean success = supplierService.delete(id);
        JSONObject obj = new JSONObject();
        obj.put("success", success);
        return obj.toString();
    }
}
