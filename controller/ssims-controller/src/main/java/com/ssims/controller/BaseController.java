package com.ssims.controller;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class BaseController extends AbstractController {
    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
        return null;
    }

    protected ModelAndView toLogInPage(HttpServletRequest request, HttpServletResponse response) {
        return layout("tiles.login", "login", request, response);
    }

    protected ModelAndView toLogOnPage(HttpServletRequest request, HttpServletResponse response) {
        return layout("tiles.logon", "login", request, response);
    }

    protected ModelAndView layout(String template, String url, HttpServletRequest request, HttpServletResponse response) {
        ModelAndView modelAndView = new ModelAndView(template);
        modelAndView.addObject("url", url);
        return modelAndView;
    }

    protected ModelAndView mainLayout(String url, HttpServletRequest request, HttpServletResponse response) {
        return layout("tiles.main", url, request, response);
    }

    protected ModelAndView adminLayout(String url, HttpServletRequest request, HttpServletResponse response) {
        return layout("tiles.main.admin", url, request, response);
    }
}
