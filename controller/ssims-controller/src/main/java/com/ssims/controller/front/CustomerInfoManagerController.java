package com.ssims.controller.front;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ssims.controller.BaseController;
import com.ssims.model.entity.CusInfoEntity;
import com.ssims.service.client.CusInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @description Created by colizajt@gmail.com on 2014/5/7.
 */
@Controller("customerInfoManagerController")
@RequestMapping("/front/fim/cim")
public class CustomerInfoManagerController extends BaseController {
    @Autowired
    private CusInfoService cusInfoService;

    @RequestMapping(params = "method=show")
    public ModelAndView show(HttpServletRequest request, HttpServletResponse response) {
        List list = cusInfoService.queryByParams();
        request.setAttribute("records", list);
        return mainLayout("front/fim/cim", request, response);
    }

    @RequestMapping(params = "method=insert")
    public
    @ResponseBody
    String insert(HttpServletRequest request, HttpServletResponse response, CusInfoEntity entity) {
        List list = cusInfoService.saveAndRefresh(entity);
        JSONArray listArray = new JSONArray();
        JSONObject obj = new JSONObject();
        if (null == list) {
            obj.put("items", null);
            return obj.toString();
        }
        listArray = JSONArray.parseArray(JSONArray.toJSONString(list));
        obj.put("items", listArray);
        return obj.toString();

    }

    @RequestMapping(params = "method=update")
    public
    @ResponseBody
    String update(HttpServletRequest request, HttpServletResponse response, CusInfoEntity entity) {
        CusInfoEntity one = cusInfoService.updateCusInfo(entity);
        JSONArray listArray = new JSONArray();
        JSONObject obj = new JSONObject();
        if (null == one) {
            obj.put("items", null);
            return obj.toString();
        }
        listArray = JSONArray.parseArray(JSONArray.toJSONString(one));
        obj.put("items", listArray);
        return obj.toString();

    }

    @RequestMapping(params = "method=delete")
    public
    @ResponseBody
    String update(HttpServletRequest request, HttpServletResponse response, @RequestParam("id") Integer id) {
        Boolean success = cusInfoService.delete(id);
        JSONObject obj = new JSONObject();
        obj.put("success", success);
        return obj.toString();
    }
}
