package com.ssims.controller.front;

import com.ssims.controller.BaseController;
import com.ssims.service.wms.StockCheckService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by coliza on 2014/6/2.
 */
@Controller("stockCheckManagerController")
@RequestMapping("/front/sc/scmt")
public class StockCheckManagerController extends BaseController {
    @Autowired
    private StockCheckService stockCheckService;

    @RequestMapping(params = "method=show")
    public ModelAndView index(HttpServletRequest request, HttpServletResponse response) {
        List list = stockCheckService.queryByParams();
        request.setAttribute("records", list);
        return mainLayout("front/sc/stcm", request, response);
    }
}
