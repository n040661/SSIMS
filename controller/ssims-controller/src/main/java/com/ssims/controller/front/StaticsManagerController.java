package com.ssims.controller.front;

import com.ssims.controller.BaseController;
import com.ssims.service.data.StaticsService;
import com.ssims.service.sale.GoodsSaleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by coliza on 2014/6/2.
 */
@Controller("staticsManagerController")
public class StaticsManagerController extends BaseController {
    @Autowired
    private GoodsSaleService goodsSaleService;
    @Autowired
    private StaticsService staticsService;

    @RequestMapping(params = "method=show")
    public ModelAndView index(HttpServletRequest request, HttpServletResponse response) {
        List list = goodsSaleService.queryByParams();

        request.setAttribute("records", list);
        return mainLayout("front/sa/sapy", request, response);
    }
}
