package com.ssims.model.entity;

/**
 * Created by coliza on 2014/5/29.
 *
 * @summary 入库记录实体类
 */
public class GoodsStockInfoEntity extends GoodsInfoEntity {
    private Integer amount;

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }
}
