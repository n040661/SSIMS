package com.ssims.security.entity;

import org.apache.shiro.authc.UsernamePasswordToken;

import java.util.Collections;
import java.util.Set;

/**
 * Created by coliza on 2015/9/1.
 */
public class SecurityUser extends UsernamePasswordToken {
    protected int id;
    protected String salt;
    protected Set<SecurityRole> roles = Collections.emptySet();
    protected Boolean locked;

    public SecurityUser() {
    }

    public SecurityUser(String username, String password) {
        super(username, password);
    }

    public Boolean getLocked() {
        return locked;
    }

    public void setLocked(Boolean locked) {
        this.locked = locked;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public Set<SecurityRole> getRoles() {
        return roles;
    }

    public void setRoles(Set<SecurityRole> roles) {
        this.roles = roles;
    }
}
