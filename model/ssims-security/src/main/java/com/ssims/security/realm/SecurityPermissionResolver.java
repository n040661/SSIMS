package com.ssims.security.realm;

import com.ssims.security.dao.SecurityPermissionDAO;
import com.ssims.security.entity.SecurityPermission;
import org.apache.shiro.authz.Permission;
import org.apache.shiro.authz.permission.PermissionResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by coliza on 2015/9/15.
 */
@Component("securityPermissionResolver")
public class SecurityPermissionResolver implements PermissionResolver {
    @Autowired
    private SecurityPermissionDAO securityPermissionDAO;

    @Override
    public Permission resolvePermission(String permission) {
        SecurityPermission securityPermission = securityPermissionDAO.findSinglePermission(permission);
        if (securityPermission != null) {
            return securityPermission;
        }
        return null;
    }
}
