/**
 * qccr.com Inc.
 * Copyright (c) 2014-2016 All Rights Reserved.
 */
package com.ssims.security.common;

/**
 * @author jiangtao
 * @version Id: Constants.java, v 0.1 $ 2016年10月28日 下午 04:51 Exp $
 */
public class Constants {
    public static final String CURRENT_USER = "curUser";
}
