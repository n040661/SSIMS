/**
 * qccr.com Inc.
 * Copyright (c) 2014-2016 All Rights Reserved.
 */
package com.ssims.security.dao;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author jiangtao
 * @version Id: BaseDAO.java, v 0.1 $ 2016年10月28日 下午 04:23 Exp $
 */
public abstract class BaseDAO {
    @Autowired
    public SqlSessionTemplate sqlSession;
}
