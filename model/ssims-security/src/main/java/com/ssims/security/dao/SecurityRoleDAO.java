package com.ssims.security.dao;

import com.ssims.security.entity.SecurityRole;
import org.springframework.stereotype.Repository;

/**
 * Created by coliza on 2015/9/10.
 */
@Repository("securityRoleDAO")
public class SecurityRoleDAO extends BaseDAO{
    public SecurityRole findByRole(String role) {
        return sqlSession.selectOne("com.ssims.security.dao.mapper.SecurityRoleMapper.queryByRole",role);
    }
}
