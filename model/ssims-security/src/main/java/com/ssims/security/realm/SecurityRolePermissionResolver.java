package com.ssims.security.realm;

import com.ssims.security.dao.SecurityRoleDAO;
import com.ssims.security.entity.SecurityPermission;
import com.ssims.security.entity.SecurityRole;
import org.apache.shiro.authz.Permission;
import org.apache.shiro.authz.permission.RolePermissionResolver;
import org.apache.shiro.authz.permission.WildcardPermission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * @description 角色权限解析器
 * Created by coliza on 2015/9/15.
 */
@Component("securityRolePermissionResolver")
public class SecurityRolePermissionResolver implements RolePermissionResolver {
    @Autowired
    private SecurityRoleDAO securityRoleDAO;
    @Override
    public Collection<Permission> resolvePermissionsInRole(String role) {
        SecurityRole securityRole = securityRoleDAO.findByRole(role);
        Set<Permission> permissionSet = new HashSet<Permission>();
        if(securityRole != null){
            Set<SecurityPermission> rolePermissions = securityRole.getPermissions();
            for(SecurityPermission item : rolePermissions){
                permissionSet.add(new WildcardPermission(item.getPermission()));
            }
            return permissionSet;
        }
        return null;
    }
}
