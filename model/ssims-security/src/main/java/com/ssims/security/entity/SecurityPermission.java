package com.ssims.security.entity;

import org.apache.shiro.authz.Permission;

import java.util.Set;

/**
 * @description 权限实体类
 * @date 2015-09-15
 */
public class SecurityPermission implements Permission {
    private int id;

    private String permission;

    private Set<SecurityRole> role;

    private String description;

    public SecurityPermission() {
    }

    public SecurityPermission(int id, String permission, String description) {
        this.id = id;
        this.permission = permission;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public Set<SecurityRole> getRole() {
        return role;
    }

    public void setRole(Set<SecurityRole> role) {
        this.role = role;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public SecurityPermission(String permission) {
        this.permission = permission;
    }

    @Override
    public boolean implies(Permission permission) {
        if (!(permission instanceof SecurityPermission)) {
            return false;
        }
        SecurityPermission other = (SecurityPermission) permission;
        if (other.getPermission() == ((SecurityPermission) permission).getPermission()) {
            return true;
        }
        return false;
    }
}
