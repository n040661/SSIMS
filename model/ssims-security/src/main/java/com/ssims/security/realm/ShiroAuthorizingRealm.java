package com.ssims.security.realm;

import com.ssims.security.common.Constants;
import com.ssims.security.common.EnAndDecryptUtils;
import com.ssims.security.dao.SecurityPermissionDAO;
import com.ssims.security.dao.SecurityRoleDAO;
import com.ssims.security.dao.SecurityUserDAO;
import com.ssims.security.entity.SecurityPermission;
import com.ssims.security.entity.SecurityRole;
import com.ssims.security.entity.SecurityUser;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.Permission;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.authz.permission.WildcardPermission;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by coliza on 2015/9/1.
 */
@Component("shiroAuthorizingRealm")
public class ShiroAuthorizingRealm extends AuthorizingRealm {

    @Autowired
    private SecurityUserDAO securityUserDAO;
    @Autowired
    private SecurityRoleDAO securityRoleDAO;
    @Autowired
    private SecurityPermissionDAO securityPermissionDAO;

    public SecurityUserDAO getSecurityUserDAO() {
        return securityUserDAO;
    }

    public void setSecurityUserDAO(SecurityUserDAO securityUserDAO) {
        this.securityUserDAO = securityUserDAO;
    }

    public SecurityRoleDAO getSecurityRoleDAO() {
        return securityRoleDAO;
    }

    public void setSecurityRoleDAO(SecurityRoleDAO securityRoleDAO) {
        this.securityRoleDAO = securityRoleDAO;
    }

    public SecurityPermissionDAO getSecurityPermissionDAO() {
        return securityPermissionDAO;
    }

    public void setSecurityPermissionDAO(SecurityPermissionDAO securityPermissionDAO) {
        this.securityPermissionDAO = securityPermissionDAO;
    }

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {

        String username  = (String)principals.fromRealm(getName()).iterator().next();
         SecurityUser user = securityUserDAO.findByUsername(username);

        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        for (SecurityRole role : user.getRoles()) {
            //基于用户名的角色信息
            String roleStr =role.getRole();
            info.addRole(roleStr);

             //基于角色的权限信息
            Set<Permission> permissionSet = new HashSet<Permission>();
            role = securityRoleDAO.findByRole(roleStr);
            Set<SecurityPermission> rolePermissions =role.getPermissions();
            for(SecurityPermission item : rolePermissions){
                permissionSet.add(new WildcardPermission(item.getPermission()));
            }
             info.setObjectPermissions(permissionSet);
        }
        return info;

    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        UsernamePasswordToken token = (UsernamePasswordToken) authenticationToken;
        token.setRememberMe(false);
        SecurityUser user = securityUserDAO.findByUsername(token.getUsername());

        if (user != null &&  String.valueOf(user.getPassword()).equals(EnAndDecryptUtils.encrypt(String.valueOf(token.getPassword()), user.getSalt()))) {
            token.setPassword(user.getPassword());
            SecurityUtils.getSubject().getSession().setAttribute(Constants.CURRENT_USER, user);
            return new SimpleAuthenticationInfo(user.getUsername(),user.getPassword(), getName());
        }
        return null;

    }
}
