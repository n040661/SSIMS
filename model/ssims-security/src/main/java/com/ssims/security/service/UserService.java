package com.ssims.security.service;

import com.ssims.common.query.Pager;
import com.ssims.security.dao.SecurityPermissionDAO;
import com.ssims.security.dao.SecurityRoleDAO;
import com.ssims.security.dao.SecurityUserDAO;
import com.ssims.security.entity.SecurityPermission;
import com.ssims.security.entity.SecurityRole;
import com.ssims.security.entity.SecurityUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Created by coliza on 2015/9/1.
 */
@Service("userService")
public class UserService {
    @Autowired
    private SecurityUserDAO securityUserDAO;
    @Autowired
    private SecurityRoleDAO securityRoleDAO;
    @Autowired
    private SecurityPermissionDAO securityPermissionDAO;

    public SecurityUser findByUsername(String username) {
        return securityUserDAO.findByUsername(username);
    }

    public SecurityRole findRole(String role) {
        return securityRoleDAO.findByRole(role);
    }

    public SecurityPermission findPermission(String permission) {
        return securityPermissionDAO.findSinglePermission(permission);
    }

    public Pager<SecurityUser> findAllUserExcludeAdmin(Map params) {
        Pager.decorateMap(params);
        params.put("notAdmin","ADMIN");
        List<SecurityUser> securityUserList =  securityUserDAO.findWithPager(params);
        return Pager.from(params, securityUserList);
    }
}
