package com.ssims.security.dao;

import com.ssims.security.entity.SecurityUser;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * Created by coliza on 2015/9/2.
 */
@Repository("securityUserDAO")
public class SecurityUserDAO extends BaseDAO {
    public SecurityUser findByUsername(String username){
        return sqlSession.selectOne("com.ssims.security.dao.mapper.SecurityUserMapper.selectOneByUsername",username);
    }

    public List<SecurityUser> findWithPager(Map params) {
        return sqlSession.selectList("com.ssims.security.dao.mapper.SecurityUserMapper.selectWithPager", params);
    }
}
