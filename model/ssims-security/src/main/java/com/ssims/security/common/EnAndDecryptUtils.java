package com.ssims.security.common;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * 安全辅助类，包括加解密
 * Created by coliza on 2015/9/8.
 */
public class EnAndDecryptUtils {
    /***
     * @param input 输入字符串
     * @return
     * @description 加密 SHA256算法
     */
    public static String encrypt(String input, String salt) {
        byte[] bytes = null;
        try {
            bytes = (input + salt).getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }

        digest.update(bytes);
        StringBuilder buf = new StringBuilder(bytes.length * 2);
        int i;
        for (i = 0; i < bytes.length; i++) {
            if (((int) bytes[i] & 0xff) < 0x10) {
                buf.append("0");
            }
            buf.append(Long.toString((int) bytes[i] & 0xff, 16));
        }
        return buf.toString();
    }

}
