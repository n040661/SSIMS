### SSIMS
一款基于SSM(springmvc＋spring＋mybatis)架构的WMS(仓库管理)系统，前端使用bootstrap＋jquery（将接入Semantic—UI），打造一款简单、实用、高效的管理系统。
### 面向用户
虽然比不上金蝶等巨头的专业软件，但适用于程序员学习分层架构和课程设计。
### 部署文档
文档在docs文件夹下
### 总体架构
项目主要分为三个部分：进货管理、库存管理、销售管理
### 更新记录

2017/3/20  更新Eclipse导入说明文档
<br/>
2017/12/18 修复了spring不能启动的bug（mybatis配置文件中mapper路径不对）
<br/>
2017/12/18 修复了部分逻辑，更新了部署文档（IDEA版）
<br/>
2018/2/1 修复了供应商、客户、商品页面逻辑，现在能正常登录，其余部分未修复
